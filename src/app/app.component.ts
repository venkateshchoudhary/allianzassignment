import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from './data.service';
import { UserModel } from './userModel'
import { NgxSpinnerService } from 'ngx-spinner';
import { CookieService } from 'ngx-cookie-service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

    registerForm: FormGroup;
    usermodels: Array<UserModel>;
    constructor(private formBuilder: FormBuilder, private http: DataService, private spinner: NgxSpinnerService, private cookieservice: CookieService) {
        this.usermodels = [];
    }
    ngOnInit() {
        this.spinner.show();

        this.registerForm = this.formBuilder.group({
            start: [''],
            end: [''],
            email: ['', [Validators.required, Validators.email]]
        });
        this.onDisplayRecords()
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onSubmit() {
        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }
        this.http.makeUserEntry(this.registerForm.value.email, this.registerForm.value.start, this.registerForm.value.end)
            .subscribe(res => {
                this.spinner.show();
                this.cookieservice.deleteAll();
                this.onDisplayRecords();
                console.log("cookie cleared ");
            },
                err => alert("Oops! something went wrong"));
        this.registerForm.reset();

    }

    onFetchFirstTenDataRecordsOfUser() {
        this.http.fetchUserDetails(this.registerForm.value.email).subscribe((res: UserModel[]) => {
            let response = JSON.stringify(res).slice(1, -1);
            //console.log("fetch response " + response);
            this.usermodels = JSON.parse(response);
        });
        this.registerForm.reset();
    }

    onDisplayRecords() {
        if (this.cookieservice.get("data")==null || this.cookieservice.get("data")=="" ) {
            this.http.displayRecords().subscribe((res: UserModel[]) => {
                let response = JSON.stringify(res).slice(1, -1);
                console.log("fetch response called");
                this.usermodels = JSON.parse(response).reverse().slice(0, 10);
                this.usermodels.slice(0, 10);
                this.cookieservice.deleteAll();
                this.cookieservice.set("data", JSON.stringify(this.usermodels),2);
                this.spinner.hide();
            });
        } else {
            this.usermodels = JSON.parse(this.cookieservice.get("data"));
            this.spinner.hide();
            console.log("fetch cookie response called");
        }

    }
}