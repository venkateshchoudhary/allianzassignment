import { Injectable } from '@angular/core';
import {
  HttpClient, HttpHeaders, HttpParams, HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor  
} from '@angular/common/http';
import { forkJoin } from 'rxjs';  // change to new RxJS 6 import syntax

import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class DataService implements HttpInterceptor {
  baseUrl: string = "http://localhost:8080/records";

  constructor(private httpClient: HttpClient) { }

  displayRecords() {
    return forkJoin(
      this.httpClient.get(this.baseUrl));
  }

  fetchUserDetails(emailAddress) {
    //console.log("Fetching data for - >" + emailAddress);
    const params = new HttpParams()
      .set('email', emailAddress)
      .set('length', "10");
    return forkJoin(
      this.httpClient.get(this.baseUrl, { params: params }));
  }

  makeUserEntry(emailAddress, startime, endtime) {
    //console.log("inserting user data email:" + emailAddress, " starttime:" + startime, " end:" + endtime);
    const formData = new FormData();
    formData.append("email", emailAddress);
    formData.append("start", startime);
    formData.append("end", endtime);
    return forkJoin(
      this.httpClient.post(this.baseUrl, formData));
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // If the call fails, retry until 5 times before throwing an error
    return next.handle(request);
  }
}
