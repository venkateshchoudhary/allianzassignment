**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).



*************************** If facing cross browser issue******************************************
URL http://localhost:4200/
## CORS is just like security most browser have to resrict its resources from outside.

1) Recommended browser (Google chrome) with extension of CORS added.
2) Open chrome browser
3) Just add CORS extension using below link

chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi/related?hl=en

*****************************************************************************

cd /tmp
$ wget http://nodejs.org/dist/v6.3.1/node-v6.3.1-linux-x64.tar.gz
$ tar xvfz node-v6.3.1-linux-x64.tar.gz
$ mkdir -p /usr/local/nodejs
$ mv node-v6.3.1-linux-x64/* /usr/local/nodejs


Add /usr/local/nodejs/bin to the PATH environment variable.

export PATH=$PATH:/usr/local/nodejs/bin


***************************** Node Js installation ************************

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

--OR--

curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs

***************************** Optional: install build tools------

To compile and install native addons from npm you may also need to install build tools:

sudo apt-get install -y build-essential

***************************** check versions of NODE and NPM **************
node -v
npm -v
***************************************************************************

***************************** Install Angular cli *************************
step 2 Install angular cli

sudo npm install -g @angular/cli

sudo npm install --save-dev @angular-devkit/build-angular

***************************** To check CLI version ***************************

ng -v

you should see something like this as output


 / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
   / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
  / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
 /_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
                |___/
    

Angular CLI: 6.1.4
Node: 10.9.0
OS: linux x64
Angular: 
... 

Package                      Version
------------------------------------------------------
@angular-devkit/architect    0.7.4
@angular-devkit/core         0.7.4
@angular-devkit/schematics   0.7.4
@schematics/angular          0.7.4
@schematics/update           0.7.4
rxjs                         6.2.2
typescript                   2.9.2


***************************************************************************


************************* Follow steps for installing docker- Link ******************

https://docs.docker.com/install/linux/docker-ce/ubuntu/#upgrade-docker-ce

=> Check version use command ->  docker --version

=> Now, follow steps mention in this link. Get docker image.( pull->run->test with given curl if working correct)

https://hub.docker.com/r/alirizasaral/timetracker/

************************ How to pull a Git project steps *********************************

1) Create any folder
2) Open terminal
3) git init
4) git remote add origin https://venkateshchoudhary@bitbucket.org/venkateshchoudhary/allianzassignment.git
5) git clone https://venkateshchoudhary@bitbucket.org/venkateshchoudhary/allianzassignment.git

************************* How to run project ?*************************************

=> Now go home directory of project
=> Open terminal->type ng serve //This starts node server
=> Now open browser localhost:4200
=> Done!!

******************************** Whats in project *************************************

=> Save user login entry
=> Fetch time log details of perticular users email id


****************************** API'S used are ****************************************


1) http://192.168.99.100:8080/records?offset=0&length=10 //GET
2) http://192.168.99.100:8080/records?email=aliriza.saral@gmail.com&length=10 //GET
3) http://192.168.99.100:8080/records //POST





************************************* SCREENSHOTS **********************************









